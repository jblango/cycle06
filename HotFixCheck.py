#https://msdn.microsoft.com/en-us/library/aa394391(v=vs.85).aspx

import sys
import win32com.client
from win32com.client import DispatchBaseClass
import string

class HotFixes(object):
    def __init__(self, logFileName):
        self.logFileName = logFileName
        
    def openLogAuditing(self,msg):
        self.fd = open(self.logFileName,"w")
        s = str(msg)
        self.fd.write(s)
        self.fd.write("\n")
        #print(self.logFileName)
        
    def writeLogAuditing(self,msg):
        s = str(msg)
        self.fd.write(s)
        self.fd.write("\n")

    def createWin32Dispatch(self,serverName):      
        self.win32Service = win32com.client.Dispatch("WbemScripting.SWbemLocator")
        self.win32Connection = self.win32Service.ConnectServer(serverName,"root\cimv2")

    
        
    def findHotFix(self,hotFixName,isLogging):
        itemList = self.win32Connection.ExecQuery("Select * from Win32_QuickFixEngineering")
        for item  in itemList:
            if (item.HotFixID == hotFixName):
                print('.................Found....................')
                self.printData(item,isLogging)
                return 1
        
        print('....NOT FOUND....')
        return 0
            
    def getHotFix(self,isLogging):
        itemList = self.win32Connection.ExecQuery("Select * from Win32_QuickFixEngineering")
        for item  in itemList:
            self.printData(item,isLogging)

    def printData(self,item,isLogging):
        try:
            if isLogging == 1:
                self.fd.write(item.CSName)
            print('Computer Name: ', item.CSName)
        except:
            print('CSName Unknown')

        try:
            if isLogging == 1:
                self.fd.write('\t')
                self.fd.write(item.HotFixID)
            print('HotFix: ',item.HotFixID)
        except:
           print('HotFixID Unknown')

        try:
            if isLogging == 1:
                self.fd.write('\t')
            print('Installed Date: ',item.InstallDate)
        except:
            print('InstallDate Unknown')

        try:
            if isLogging == 1:
                self.fd.write('\t')
                self.fd.write(item.InstalledBy)
            print('Installed By: ',item.InstalledBy)
        except:
            print('InstalledBy Unknown ')

        try:
            if isLogging == 1:
                self.fd.write('\t')
                self.fd.write(item.InstalledOn)
            print('Installed On: ',item.InstalledOn)
        except:
            print('InstalledOn Unknown ')

        try:
            if isLogging == 1:
                self.fd.write('\t')
                self.fd.write(item.Description)
            print('Description: ',item.Description)
        except:
            print('Description Unknown ')
            
        if isLogging == 1:
            self.fd.write('\n')
        print('\n')

    def closeFile(self):
        self.fd.close


def main():
    isLogging = 0;
    server='127.0.0.1'
    #print(len(sys.argv))
    if (len(sys.argv) != 2 and len(sys.argv) != 3):
        print('[+]...To list all hotfix enter...')
        print("\tHotFixCheck.py IPADDRESS")
        #print('\n')
        print('[+]...To Search for an installed hotfix enter...')
        print("\tHotFixCheck.py IPADDRESS searchID")
        sys.exit("Exiting...")
        
    print('Please wait...')
    
    hf = HotFixes('logHotFile.txt')
    
    if len(sys.argv) == 2:
        isLogging = 1;
        server = sys.argv[1]
    elif len(sys.argv) == 3:
        isLogging = 0
        server = sys.argv[1]
        searchID = sys.argv[2]
        print('Searching for =>',searchID)
        
    if (isLogging == 1):
        hf.openLogAuditing('-----------Start Logging------------------')
        
    hf.createWin32Dispatch(server)

    if (isLogging == 1):
        hf.getHotFix(isLogging)
        hf.closeFile()
    else:
        hf.findHotFix(searchID,isLogging)


if __name__ == "__main__":
    main()

